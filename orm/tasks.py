import logging


logger = logging.getLogger(__name__)

class SessionTask(object):
    def __init__(self, request):
        self.session = request.session
        self.task = self.session.setdefault('tasks', {'list': [], 'details': {}})
    
    def add(self, context, code):
        state, task = context['success_info'], context['current_task']
        self.task['details'][str(task.id)] = {'code': code, 'level': task.level, 'state': state}
        list = self.task['list']
        
        if state and task.id not in list:
            list.append(task.id)
        elif not state and task.id in list:
            list.remove(task.id)
        else:
            pass
            
        self.save()
    
    def check_task(self, id):
        for idx in self.task.values():
            if id in idx:
                return True
        return False

    def clear(self):
        self.session.clear()
        self.task['details'].clear()
        self.task['list'].clear()
        self.save()
    
    def get_task(self, task):
        return self.task['details'].get(str(task.id), {})
    
    def save(self):
        self.session.modified = True

    
