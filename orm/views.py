import logging
from django.shortcuts import redirect, render

from .forms import CodeForm
from .models import tasks, validator
from .tasks import SessionTask


logger = logging.getLogger(__name__)


def main(request, id=None):
    session = SessionTask(request)
    context = tasks.get_context_tasks(id, session)
    context['success_info'] = None 
    context['current_session_task'] = session.get_task(context['current_task'])
    context['session'] = session
    
    if request.method == 'POST':
        user_code = request.POST.get('code')
        print(user_code)
        context['form'] = CodeForm(request.POST)
        context['success_info'] = validator.CheckCode(request, context).check()
        context['current_session_task']['state'] = context['success_info']
        session.add(context, user_code)
        
    else:
        if context['current_session_task']:
            context['form'] = CodeForm(initial={'code': context['current_session_task']['code']})
        else:
            context['form'] = CodeForm()
    return render(request, 'apps/orm/main.html', context)


def clear(request):
    session = SessionTask(request)
    session.clear()
    return redirect('orm:main')

 

