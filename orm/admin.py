from django.contrib import admin

from orm.models.models import Task, Result, Book, Author, Category


@admin.register(Result)
class ResultAdmin(admin.ModelAdmin):
    fields = ['code', 'task']

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    fields = ['title', 'level', 'special_task']


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    fields = ['name', 'category', 'price', 'author']
    filter_horizontal = ('author',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    fields = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    fields = ['first_name', 'last_name']
