from django.test import TestCase

from orm.models.models import Book, Category, Author, Task, Result

class OrmModelsTest(TestCase):
    def setUp(self):
        self.author = Author.objects.create(
            first_name='Aleksander',
            last_name='Wiedeńśki'
            )
        self.category = Category.objects.create(
            name='Fantasy',
            slug='fantasy'
            )
        self.book = Book.objects.create(
            category=self.category,
            name='The Lord of Rings',
            price=87.99
            )
        self.book.author.add()
        self.task = Task.objects.create(
            title='Type a command that retrieves all book model from the database',
            level='easy'
        )
        self.result = Result.objects.create(
            code='Book.objects.all()',
            task=self.task
        )

    def test_author(self):
        number_objects = Author.objects.count()
        self.assertEqual(number_objects, 1)

    def test_category(self):
        number_objects = Category.objects.count()
        self.assertEqual(number_objects, 1)
    
    def test_book(self):
        number_objects = Book.objects.count()
        self.assertEqual(number_objects, 1)
    
    def test_result(self):
        number_objects = Result.objects.count()
        self.assertEqual(number_objects, 1)
    
    def test_task(self):
        number_objects = Task.objects.count()
        self.assertEqual(number_objects, 1)
    
    def test_task_get_absolute_url(self):
        response = self.client.get(self.task.get_absolute_url())
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Type a command that retrieves all')
