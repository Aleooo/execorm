from django.contrib import messages
from django.http import HttpRequest
from django.test import TestCase

from orm.models.models import Book, Category, Author, Task, Result
from orm.models.validator import CheckCode

class OrmModelsTest(TestCase):
    def setUp(self):
        self.author = Author.objects.create(
            first_name='Aleksander',
            last_name='Wiedeńśki'
            )
        self.category = Category.objects.create(
            name='Fantasy',
            slug='fantasy'
            )
        self.book = Book.objects.create(
            category=self.category,
            name='The Lord of Rings',
            price=87.99
            )
        self.book.author.add()
        self.task = Task.objects.create(
            title='Type a command that retrieves all book model from the database',
            level='easy'
        )
        self.result = Result.objects.create(
            code='Book.objects.all()',
            task=self.task
        )
        self.request = HttpRequest()
        self.request._messages = messages.storage.default_storage(self.request)

    def test_checkcode(self):
        check_object = CheckCode(self.request, 'Book.objects.all()', self.result.code)

        self.assertEqual(check_object.request, self.request)
        self.assertEqual(check_object.user_code, 'Book.objects.all()')
        self.assertEqual(check_object.correct_code, 'Book.objects.all()')

    def test_validate_name_1(self):
        check_object = CheckCode(self.request, 'Book.objects.all()', self.result.code)
        self.assertTrue(check_object.validate_name())
    
    def test_validate_name_2(self):
        check_object = CheckCode(self.request, '.objects.all()', self.result.code)
        self.assertFalse(check_object.validate_name())
    
    def test_validate_name_3(self):
        check_object = CheckCode(self.request, 'Book.ects.all()', self.result.code)
        self.assertFalse(check_object.validate_name())
    
    def test_validate_name_4(self):
        check_object = CheckCode(self.request, 'Book.objects.all', self.result.code)
        self.assertTrue(check_object.validate_name())
    
    def test_validate_name_5(self):
        check_object = CheckCode(self.request, 'Author.objects.all()', self.result.code)
        self.assertTrue(check_object.validate_name())
    
    def test_validate_name_6(self):
        check_object = CheckCode(self.request, 'Category.objects.all()', self.result.code)
        self.assertTrue(check_object.validate_name())
    
    def test_forbidden_methods_1(self):
        check_object = CheckCode(self.request, 'Book.objects.all()', self.result.code)
        self.assertTrue(check_object.forbidden_methods())
    
    def test_forbidden_methods_2(self):
        check_object = CheckCode(self.request, 'Book.objects.create()', self.result.code)
        self.assertFalse(check_object.forbidden_methods())
    
    def test_forbidden_methods_3(self):
        check_object = CheckCode(self.request, 'Book.objects.update()', self.result.code)
        self.assertFalse(check_object.forbidden_methods())
    
    def test_forbidden_methods_4(self):
        check_object = CheckCode(self.request, 'Book.objects.delete()', self.result.code)
        self.assertFalse(check_object.forbidden_methods())
    
    def test_forbidden_methods_5(self):
        check_object = CheckCode(self.request, 'Book.objects.get(name="delete")', self.result.code)
        self.assertTrue(check_object.forbidden_methods())
    
    def test_queryset_1(self):
        check_object = CheckCode(self.request, 'Book.objects.all()', self.result.code)
        self.assertTrue(check_object.queryset())
        self.assertEqual(len(check_object.user_objects), 1)
        self.assertEqual(len(check_object.correct_objects), 1)
    
    def test_queryset_2(self):
        check_object = CheckCode(self.request, 'Book.objects.all)', self.result.code)
        self.assertFalse(check_object.queryset())
    
    def test_queryset_3(self):
        check_object = CheckCode(self.request, 'Bookobjects.all()', self.result.code)
        self.assertFalse(check_object.queryset())
    
    def test_check_1(self):
        check_object = CheckCode(self.request, 'Book.objects.all()', self.result.code)
        self.assertTrue(check_object.check())
    
    def test_check_2(self):
        check_object = CheckCode(self.request, 'Book.objects.first()', self.result.code)
        self.assertFalse(check_object.check())
    
    def test_check_3(self):
        check_object = CheckCode(self.request, 'Book.objectsall()', self.result.code)
        self.assertFalse(check_object.check())
    
    def test_check_4(self):
        check_object = CheckCode(self.request, 'Books.objects.all()', self.result.code)
        self.assertFalse(check_object.check())
    
    def test_check_5(self):
        check_object = CheckCode(self.request, 'Books.objects.create()', self.result.code)
        self.assertFalse(check_object.check())

