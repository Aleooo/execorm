from django import forms

class CodeForm(forms.Form):
    code = forms.CharField(max_length=300, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'autocomplete': 'off',
        }
    ))