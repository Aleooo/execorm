from django.db import models
from django.urls import reverse


class Author(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self) -> str:
        return f'{self.first_name} {self.last_name}'

class Category(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField()

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

class Book(models.Model):
    author = models.ManyToManyField(Author, related_name='books')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='books')
    name = models.CharField(max_length=250)
    price = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return self.name

class BookP(Book):
    class Meta:
        proxy = True

    def __str__(self):
        return self.name


TASK_LEVELS = [
    ('easy', 'Easy'),
    ('medium', 'Medium'),
    ('hard', 'Hard'),
 ]
SPECIAL_TASKS = [
    ('create', 'Create'),
    ('delete', 'Delete'),
    ('none', 'None'),
    ('random', 'Random'),
    ('update', 'Update'),
]

class Task(models.Model):
    title = models.CharField(max_length=1000)
    level = models.CharField(max_length=10, choices=TASK_LEVELS)
    special_task = models.CharField(max_length=10, choices=SPECIAL_TASKS, default='none')

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('orm:task', args=[self.pk])


class Result(models.Model):
    code = models.CharField(max_length=200)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='result')

    def __str__(self):
        return self.code
    



