import logging
from django.contrib import messages
from django.db.models import Q
from django.db.models.functions import Length

from .models import Book, Author, Category


logger = logging.getLogger(__name__)


class CheckCode(object):
    FORBIDDEN_METHODS = {
        'create': '.create(',
        'update': '.update(',
        'delete': '.delete(',
    }
    VALIDATE_NAMES = {
        'model': ['Book', 'Author', 'Category'],
        'manager': ['.objects', '.book', '.book_set'],
    }
    
    def __init__(self, request, context):
        self.correct_code = context['current_task'].result.first().code
        self.correct_objects = None
        self.request = request
        self.special_task = context['current_task'].special_task
        self.user_code = request.POST.get('code')
        self.user_objects = None

        if self.special_task == 'none':
            self.special_task = None
        else:
            self.correct_code = context['current_task'].result.all()
    
    def check(self):
        if not self.validate_name():
            return False
        
        if self.special_task:
            return self.special()

        if not self.forbidden_methods():
            return False

        if not self.queryset():
            return False
        try: 
            return self.user_objects == self.correct_objects
        except:
            messages.info(self.request, 'Invalid code')
            return False
    
    def validate_name(self):
        for key, value in self.VALIDATE_NAMES.items():
            aux = True
            for name in value:
                if name in self.user_code:
                    aux = False
                    break
            if aux:
                messages.info(self.request, f'Incorrect {key}')
                return False
        return True
    
    def forbidden_methods(self):
        # checks the code which not modify the database.
        for key, value in self.FORBIDDEN_METHODS.items():
            if value in self.user_code:
                messages.info(self.request, f'In this task we don\'t need to {key}')
                return False
        return True
    
    def queryset(self):
        try:
            self.user_objects = eval(self.user_code).query.__str__()
        except:
            try:
                self.user_objects = eval(self.user_code)
            except:
                messages.info(self.request, 'Incorrect syntax')
                return False

        try:
            self.correct_objects = eval(self.correct_code).query.__str__()
        except:
            try:
                self.correct_objects = eval(self.correct_code)
            except:
                messages.info(self.request, 'Program error, we apologize for the inconvenience :(')
                return False
        return True
    
    def special(self):
        if self.special_task == 'random':
            for correct in self.correct_code:
                if correct.code == self.format_user_code():
                    return True
        return False
    
    def format_user_code(self):
        return self.user_code.replace('\'', '\"')


