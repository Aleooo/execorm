from django.db.models import Q

from .models import Task


TASK_LEVELS = ['easy', 'medium', 'hard']

# The function retrieves tasks they aren't yet completed 
def get_context_tasks(id, session):
        idx_list_session = session.task['list']
        context = {'current_task': None}
        context['tasks'] = sorted_tasks()

        # iterates through task levels and retrieves objects from database they aren't completed
        for task_level in TASK_LEVELS:
            task_objects = Task.objects.filter(Q(level=task_level))
            context[task_level] = task_objects
        
        # when request refers to specific task. It retrieves from database and
        # returns all tasks and specific task if there is request
        if id:
            try:
                current_task = Task.objects.get(id=id)
            except Task.DoesNotExist:
                return optional_task(context, idx_list_session)

            context['current_task'] = current_task
            return context
        else:
            return optional_task(context, idx_list_session)
            

# The function gets any task to display in main site if any are
def optional_task(context, session):
    for task in context['tasks']:
        if task.id not in session:     
            context['current_task'] = task
            return context
    context['current_task'] = context['tasks'][0]
    return context


def sorted_tasks():
    tasks = Task.objects.all()
    order = { key: i for i, key in enumerate(TASK_LEVELS)}
    tasks = sorted(tasks, key=lambda t: order[t.level])
    return tasks