from django.urls import path

from orm import views

app_name = 'orm'

urlpatterns = [
    path('', views.main, name='main'),
    path('clear/', views.clear, name='clear'),
    path('<int:id>/', views.main, name='task')
]