# Generated by Django 4.0.2 on 2022-02-28 21:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orm', '0004_task_special'),
    ]

    operations = [
        migrations.RenameField(
            model_name='task',
            old_name='special',
            new_name='special_task',
        ),
    ]
